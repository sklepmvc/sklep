﻿using Sklep.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Sklep
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Application_Start()
        {
            HitCounter hc = db.HitCounters.First();
            Application["Totaluser"] = hc.Counter;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {
            Application.Lock();
            HitCounter hc = db.HitCounters.First();
            hc.Counter = hc.Counter + 1;
            db.SaveChanges();
            Application["Totaluser"] = hc.Counter;
            Application.UnLock();
        }
    }
}
