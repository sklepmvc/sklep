﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Postal;
using Sklep.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Sklep.Controllers
{
    public class ShoppingCartController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        private string strCart = "Cart";
       
        // GET: ShoppingCart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowImage(int id)
        {
            Product product = db.Products.Find(id);
            var imageData = product.Image;
            FileContentResult result = null;
            try { result = File(imageData, "image/jpg"); } catch { }
            if (result != null) return result;
            else return File("~/img/square.png", "image/jpg");
        }

        public ActionResult OrderNow(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if(Session["Cart"] == null)
            {
                List<Sale> lsCart = new List<Sale>
                {
                    new Sale { Product = db.Products.Find(id), ProductID = db.Products.Find(id).ProductID, Quantity = 1 }
                };
                
            Session["Cart"] = lsCart;
            }
            else
            {
                List<Sale> lsCart = (List<Sale>)Session["Cart"];
                int check = IsExistCheck(id);

                if (check == -1)
                    lsCart.Add(new Sale { Product = db.Products.Find(id), ProductID = db.Products.Find(id).ProductID, Quantity = 1 });
                else
                    lsCart[check].Quantity++;

                Session["Cart"] = lsCart;
            }
            return View("Index");
        }

        public ActionResult OrderAgain(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = db.Orders.Find(id);
            var sales = db.Sales.Where(x => x.OrderID == id).ToList();

            if (Session["Cart"] == null)
            {
                List<Sale> lsCart = new List<Sale>();
                foreach (var sale in sales)
                {
                    lsCart.Add(new Sale { Product = sale.Product, ProductID = sale.ProductID, Quantity = sale.Quantity });
                }

                Session["Cart"] = lsCart;
            }
            else
            {
                List<Sale> lsCart = (List<Sale>)Session["Cart"];
                int check = IsExistCheck(id);

                foreach (Sale sale in sales)
                {
                    if (check == -1)
                        lsCart.Add(new Sale { Product = sale.Product, ProductID = sale.ProductID, Quantity = sale.Quantity });
                    else
                        lsCart[check].Quantity += sale.Quantity;
                }

                Session["Cart"] = lsCart;
            }
            return View("Index");
        }

        private int IsExistCheck(int? id)
        {
            List<Sale> lsCart = (List<Sale>)Session[strCart];
            for (int i = 0; i < lsCart.Count; i++)
            {
                if (lsCart[i].Product.ProductID == id) return i;
            }
            return -1;
        }

        private string GetProductName(int? id)
        {
            List<Sale> lsCart = (List<Sale>)Session[strCart];
            var productName = "";
            for (int i = 0; i < lsCart.Count; i++)
            {
                if (lsCart[i].Product.ProductID == id) productName = lsCart[i].Product.Name;
            }
            return productName;
        }

        public ActionResult Delete(int? id)
        {
            int check = IsExistCheck(id);
            List<Sale> lsCart = (List<Sale>)Session["Cart"];


            lsCart.RemoveAt(check);
            var result = new ShoppingCartViewModel
            {
                Message = Server.HtmlEncode(GetProductName(id)) +
                        " has been removed from your shopping cart.",
                CartTotal = lsCart.Sum(x => x.Quantity * x.Product.Price),
                CartCount = lsCart.Count,
                ItemCount = check
            };

            return Json(result);

        }

        public ActionResult AddOne(int? id)
        {
                int check = IsExistCheck(id);
                List<Sale> lsCart = (List<Sale>)Session["Cart"];

                lsCart[check].Quantity++;

            var result = new ShoppingCartViewModel
            {
                CartTotal = lsCart.Sum(x => x.Quantity * x.Product.Price),
                ItemCount = lsCart[check].Quantity,
                ItemPrice = lsCart[check].Product.Price
            };

            return Json(result);
        }

        public ActionResult RemoveOne(int? id)
        {
                int check = IsExistCheck(id);
                List<Sale> lsCart = (List<Sale>)Session["Cart"];
                
             lsCart[check].Quantity--;


            var result = new ShoppingCartViewModel
            {
                CartTotal = lsCart.Sum(x => x.Quantity * x.Product.Price),
                ItemCount = lsCart[check].Quantity,
                ItemPrice = lsCart[check].Product.Price
            };

            return Json(result);
        }

        public async System.Threading.Tasks.Task<ActionResult> Buy()
        {
            List<Sale> lsCart = (List<Sale>)Session["Cart"];
            string message = "";
            int available = 1;
            foreach (var sale in lsCart)
            {
                if (checkProductsQuantity(sale) != 0)
                {
                    available = 0;
                    int value = (checkProductsQuantity(sale) - sale.Quantity)*(-1);
                    message = message + "\\n" + sale.Product.Name + " - remove " + value;
                }
            }
            if (available == 0)
            {
                string alert = "<script language='javascript' type='text/javascript'>alert('Not enough articles in store!" + message + "');</script>";
                TempData["msg"] = alert;
                return View("Index");
            }
            else if (Request.IsAuthenticated)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                return View("Buy");
            }
            else
                return RedirectToAction("Login", "Account", new { returnurl = Url.Action("Pay", "ShoppingCart") });
        }

        public int checkProductsQuantity(Sale sale)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            if (db.Products.Find(sale.Product.ProductID).StockStatus < sale.Quantity)
                return db.Products.Find(sale.Product.ProductID).StockStatus;
            else return 0;
        }

        public ActionResult Pay()
        {
            List<Sale> lsCart = (List<Sale>)Session["Cart"];

            if (Request.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                var applicationUser = db.Users.FirstOrDefault(p => p.Id == userId);
                var userModel = new UserOrderViewModel
                {
                    Name = applicationUser.Name,
                    Surname = applicationUser.Surname,
                    Address = applicationUser.Address,
                    ZipCode = applicationUser.ZipCode,
                    City = applicationUser.City,
                    Country = applicationUser.Country,
                    Email = applicationUser.Email,
                    Phone = applicationUser.PhoneNumber,
                    Sales = ((List<Sale>)Session["Cart"])
                };

                if (applicationUser == null)
                {
                    return HttpNotFound();
                }
                else return View(userModel);
            }
            else return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Pay([Bind(Include = "OrderID,PaymentMethod,ShipmentMethod")] Order order)
        {
            List<Sale> lsCart = (List<Sale>)Session["Cart"];
            ApplicationDbContext db = new ApplicationDbContext();

            if (Request.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                var applicationUser = db.Users.FirstOrDefault(p => p.Id == userId);

                order.Date = System.DateTime.Now;
                order.State = State.Started;
                order.UserId = userId;

                db.Orders.Add(order);
                db.SaveChanges();

                for (int i = 0; i < lsCart.Count; i++)
                {
                    var newSale = new Sale();
                    newSale.ProductID = lsCart[i].ProductID;
                    newSale.Quantity = lsCart[i].Quantity;
                    newSale.Order = order;
                    newSale.OrderID = newSale.Order.OrderID;
                    db.Sales.Add(newSale);
                }

               

                foreach (var element in lsCart)
                {
                    db.Products.Find(element.ProductID).StockStatus = (db.Products.Find(element.ProductID).StockStatus - element.Quantity);
                    db.Products.Find(element.ProductID).SoldNumber = (db.Products.Find(element.ProductID).SoldNumber + element.Quantity);

                }
                db.SaveChanges();
                var newOrder = db.Orders.Include("Sales").Include("Sales.Product").SingleOrDefault(o => o.OrderID == order.OrderID);

                OrderConfirmationEmail email = new OrderConfirmationEmail();
                email.To = applicationUser.Email;
                email.From = "c2crental.adm@gmail.com";
                email.Subject = "Order confirmation";
                email.OrderID = newOrder.OrderID;
                email.Sales = newOrder.Sales;
                email.State = newOrder.State;
                email.Send();


                return RedirectToAction("Finish", "ShoppingCart");
            }
            else   return HttpNotFound();
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Finish()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Sale> lsCart = (List<Sale>)Session["Cart"];

          
            ((List<Sale>)Session["Cart"]).Clear();

           

            return View("Finish");
        }
    }
}