﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExportToExcel.Domain;
using Sklep.Models;
using System.Data.Entity;
using System.Net;

namespace ExportToExcel.Controllers
{
    public class PdfGenerationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PdfGeneration
        public ActionResult GeneratePdf(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            var query = category.Products.Select(b => b.Name + " - " + b.Price + " PLN");
            ViewBag.Products = query.ToList();

            return View(category);
        }

        [HttpPost]
        public ActionResult GeneratePdf(int? id, string heading, string paragraph)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            var query = category.Products.Select(b => b.Name + " - " + b.Price + " PLN");
            ViewBag.Products = query.ToList();

            PdfUtil objPdf = new PdfUtil();

            string path = Server.MapPath("~/Reports/price-list.pdf");
            if (!Directory.Exists(Server.MapPath("~/Reports")))
                Directory.CreateDirectory(Server.MapPath("~/Reports"));


            iTextSharp.text.Rectangle pagesize = new iTextSharp.text.Rectangle(20, 20, PageSize.A4.Width, PageSize.A4.Height);
            Document doc = new Document(pagesize, 50, 50, 50, 50);
            MemoryStream ms = new MemoryStream();
            PdfWriter pw = PdfWriter.GetInstance(doc, ms);
            doc.Open();

            doc.Add(objPdf.AddParagraphHeader(heading));
            doc.Add(objPdf.AddParagragh(paragraph));

            doc.Close();
            byte[] byteArray = ms.ToArray();
            ms.Flush();
            ms.Close();
            ms.Dispose();
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=price-list.pdf");
            Response.AddHeader("Content-Length", byteArray.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(byteArray);
            return View(category);
        }
    }
}