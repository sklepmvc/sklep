﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sklep.Models;
using System.Globalization;
using PagedList;
using Microsoft.AspNet.Identity;
using System.Web.Helpers;
using System.Drawing;

namespace Sklep.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.Category);
            return View(products.ToList());
        }

        public ActionResult ShowImage(int id)
        {
            Product product = db.Products.Find(id);
            var imageData = product.Image;
            FileContentResult result = null;
            try { result = File(imageData, "image/jpg"); } catch { }
            if (result != null) return result;
            else return File("~/img/square.png", "image/jpg");
        }

        public ActionResult ShowBigImage(int id)
        {
            Product product = db.Products.Find(id);
            var imageData = product.BigImage;
            FileContentResult result = null;
            try { result = File(imageData, "image/jpg"); } catch { }
            if (result != null) return result;
            else return File("~/img/square.png", "image/jpg");
        }

        public ActionResult AllProducts(string searchString, string keyWord, string producer, string priceFrom, string priceTo, string category, string sortOrder, int page=1)
        {
            var products = db.Products.Include(p => p.Category);
            var categories = db.Categories;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString));
            }

            if (!String.IsNullOrEmpty(keyWord))
            {
                products = products.Where(p => p.Name.Contains(keyWord)
                               || p.Description.Contains(keyWord));
            }

            if (category != "---")
            {
                try
                {
                    int catID = int.Parse(category);
                    products = products.Where(p => p.CategoryID == catID);
                }
                catch { }
            }   

            if (!String.IsNullOrEmpty(producer))
            {
                products = products.Where(p => p.Producer.Contains(producer));
            }

            if (!String.IsNullOrEmpty(priceFrom))
            {
                float floatPriceFrom = float.Parse(priceFrom, CultureInfo.InvariantCulture);
                products = products.Where(p => p.Price >= floatPriceFrom);
            }

            if (!String.IsNullOrEmpty(priceTo))
            {
                float floatPriceTo = float.Parse(priceTo, CultureInfo.InvariantCulture);
                products = products.Where(p => p.Price <= floatPriceTo);
            }

            ViewBag.Category = new SelectList(db.Categories, "CategoryID", "Name");

            products = products.OrderByDescending(s => s.Name);
            int pageSize;

            var userId = User.Identity.GetUserId();
            var currentUser = db.Users.FirstOrDefault(p => p.Id == userId);
            if (currentUser == null) { pageSize = 10; }
            else pageSize = currentUser.ProductsOnPage;

            if (pageSize == 0)
            {
                pageSize = 10;
                currentUser.ProductsOnPage = 10;
                db.SaveChanges();
            }

            PagedList<Product> model = new PagedList<Product>(products, page, pageSize);

            return View(model);
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if(product!=null) ViewBag.Description = product.Description;
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name");
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Image,BigImage")] Product product, HttpPostedFileBase image, HttpPostedFileBase bigImage)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    WebImage img = new WebImage(image.InputStream);
                    if (img.Width > 500 || img.Height > 500)
                        img.Resize(500, 500);
                    byte[] obrazek = img.GetBytes();
                    product.Image = obrazek;
                }
                if (bigImage != null)
                {
                    WebImage img1 = new WebImage(bigImage.InputStream);
                    if (img1.Width > 800 || img1.Height > 800)
                        img1.Resize(800, 800);
                    byte[] obrazek1 = img1.GetBytes();
                    product.BigImage = obrazek1;
                }

                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", product.CategoryID);
            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", product.CategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "Image,BigImage")] Product product, HttpPostedFileBase image, HttpPostedFileBase bigImage, bool deleteImage = false, bool deleteBigImage = false)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;

                if (deleteImage == true) product.Image = null;
                if (deleteBigImage == true) product.BigImage = null;

                if (image != null)
                {
                    WebImage img = new WebImage(image.InputStream);
                    if (img.Width > 500 || img.Height > 500)
                        img.Resize(500, 500);
                    byte[] obrazek = img.GetBytes();
                    product.Image = obrazek;
                }
                else
                {
                    db.Entry(product).Property(m => m.Image).IsModified = false;
                }
                if (deleteImage == true) db.Entry(product).Property(m => m.Image).IsModified = true;

                if (bigImage != null)
                {
                    WebImage img1 = new WebImage(bigImage.InputStream);
                    if (img1.Width > 800 || img1.Height > 800)
                        img1.Resize(800, 800);
                    byte[] obrazek1 = img1.GetBytes();
                    product.BigImage = obrazek1;
                }
                else
                {
                    db.Entry(product).Property(m => m.BigImage).IsModified = false;
                }
                if (deleteBigImage == true) db.Entry(product).Property(m => m.BigImage).IsModified = true;

            db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", product.CategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
