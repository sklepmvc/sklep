﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sklep.Models;
using Microsoft.AspNet.Identity;

namespace Sklep.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string strCart = "Cart";

        // GET: Orders
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.ApplicationUser);
            return View(orders.ToList());
        }

        // GET: Orders
        [Authorize(Roles = "admin, user")]
        public ActionResult MyOrders()
        {
            var orders = db.Orders.Include(o => o.ApplicationUser);
            var userid = User.Identity.GetUserId();
            var query = db.Orders.Where(o => o.UserId == userid);
            ViewBag.MyOrders = query.ToList();
            return View(query.ToList());
        }

        // GET: Orders/Details/5
        [Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            int orderID = order.OrderID;

            if (order == null)
            {
                return HttpNotFound();
            }
            var query = order.Sales;
            ViewBag.Sales = query.ToList();

            if(db.Sales.FirstOrDefault(x => x.OrderID == orderID) != null)
            {
                var total = String.Format("{0:N0}", db.Sales.Where(x => x.OrderID == orderID).Sum(x => x.Quantity * x.Product.Price));
                ViewBag.Total = total;
            }
            else ViewBag.Total = "0,00";

            return View(order);
        }

        // GET: Orders/MyOrders/OrderDetails/5
        [Authorize(Roles = "admin, user")]
        public ActionResult OrderDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            int orderID = order.OrderID;

            var userid = User.Identity.GetUserId();
            if (order.UserId!= userid)
            {
                return HttpNotFound();
            }
            if (order == null)
            {
                return HttpNotFound();
            }
            var query = order.Sales;
            ViewBag.Sales = query.ToList();

            if (db.Sales.FirstOrDefault(x => x.OrderID == orderID) != null)
            {
                var total = String.Format("{0:N0}", db.Sales.Where(x => x.OrderID == orderID).Sum(x => x.Quantity * x.Product.Price));
                ViewBag.Total = total;
            }
            else ViewBag.Total = "0,00";

            return View(order);
        }

        // GET: Orders/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderID,Date,PaymentMethod,ShipmentMethod,State")] Order order, string id)
        {
            if (ModelState.IsValid)
            {
                order.UserId = id;
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", order.UserId);
            return View(order);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order order, string userId)
        {
            var currentOrder = db.Orders.FirstOrDefault(p => p.OrderID == order.OrderID);
            if (currentOrder == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                currentOrder.UserId = userId;
                currentOrder.Date = order.Date;
                currentOrder.PaymentMethod = order.PaymentMethod;
                currentOrder.ShipmentMethod = order.ShipmentMethod;

                if(currentOrder.State!=order.State)
                {
                    currentOrder.State = order.State;
                    db.SaveChanges();

                    var applicationUser = db.Users.FirstOrDefault(p => p.Id == currentOrder.UserId);

                    OrderConfirmationEmail email = new OrderConfirmationEmail();
                    email.To = applicationUser.Email;
                    email.From = "c2crental.adm@gmail.com";
                    email.Subject = "Order status changed";
                    email.OrderID = currentOrder.OrderID;
                    email.Sales = currentOrder.Sales;
                    email.State = currentOrder.State;
                    email.Send();
                }
                
            return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
