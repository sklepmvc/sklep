﻿using Sklep.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sklep.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            List<Product> list = db.Products.ToList();
            list.Sort((a, b) => a.SoldNumber.CompareTo(b.SoldNumber));
            list = list.OrderBy(i => i.SoldNumber).ToList();
            list.Reverse();

            while (list.Count() > 10)
            {
                list.Remove(list.Last());
            }
            
            return View(list);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Who we are?";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us";

            return View();
        }
    }
}