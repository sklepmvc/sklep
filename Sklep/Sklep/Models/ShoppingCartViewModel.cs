﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sklep.Models
{
    public class ShoppingCartViewModel
    {
        public string Message { get; set; }
        public float CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public float ItemPrice { get; set; }
    }
}