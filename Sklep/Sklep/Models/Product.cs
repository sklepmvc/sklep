﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sklep.Models
{
    public class Product
    {
        public int ProductID { get; set; }

        public string Name { get; set; }

        public float Price { get; set; }

        public string Producer { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public int StockStatus { get; set; }

        public byte[] Image { get; set; }

        public byte[] BigImage { get; set; }

        public int CategoryID { get; set; }

        public int SoldNumber { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

    }
}