﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sklep.Models
{
    public class Order
    {
        public int OrderID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public ShipmentMethod ShipmentMethod { get; set; }

        public State State { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }
    }

    public enum PaymentMethod
    {
        Cash,
        CreditCard,
        BankTransfer
    }

    public enum ShipmentMethod
    {
        Economy,
        Priority,
        Courier,
        Personally
    }

    public enum State
    {
        Started,
        InProgress,
        Finished,
        Canceled
    }
}