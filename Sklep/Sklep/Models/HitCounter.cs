﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sklep.Models
{
    public class HitCounter
    {
        public int HitCounterID { get; set; }
        public int Counter { get; set; }
    }
}