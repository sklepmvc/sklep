﻿using Postal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sklep.Models
{
    public class OrderConfirmationEmail : Email
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public int OrderID { get; set; }
        public State State { get; set; }
        public ICollection<Sale> Sales { get; set; }
       

    }
}