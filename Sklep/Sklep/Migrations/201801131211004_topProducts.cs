namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class topProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "SoldNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "SoldNumber");
        }
    }
}
