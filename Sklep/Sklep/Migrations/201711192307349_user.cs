namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Product_ProductID", "dbo.Products");
            DropForeignKey("dbo.Orders", "CustomerID", "dbo.Customers");
            DropIndex("dbo.Orders", new[] { "CustomerID" });
            DropIndex("dbo.Orders", new[] { "Product_ProductID" });
            DropPrimaryKey("dbo.Customers");
            AddColumn("dbo.Orders", "Customer_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Customers", "UserId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Customers", "ZipCode", c => c.String());
            AddColumn("dbo.Sales", "Quantity", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "Surname", c => c.String());
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            AddColumn("dbo.AspNetUsers", "ZipCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
            AddColumn("dbo.AspNetUsers", "Country", c => c.String());
            AlterColumn("dbo.Customers", "CustomerID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Customers", "UserId");
            CreateIndex("dbo.Orders", "Customer_UserId");
            CreateIndex("dbo.Customers", "UserId");
            AddForeignKey("dbo.Customers", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Orders", "Customer_UserId", "dbo.Customers", "UserId");
            DropColumn("dbo.Orders", "Product_ProductID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Product_ProductID", c => c.Int());
            DropForeignKey("dbo.Orders", "Customer_UserId", "dbo.Customers");
            DropForeignKey("dbo.Customers", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Customers", new[] { "UserId" });
            DropIndex("dbo.Orders", new[] { "Customer_UserId" });
            DropPrimaryKey("dbo.Customers");
            AlterColumn("dbo.Customers", "CustomerID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.AspNetUsers", "Country");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "ZipCode");
            DropColumn("dbo.AspNetUsers", "Address");
            DropColumn("dbo.AspNetUsers", "Surname");
            DropColumn("dbo.AspNetUsers", "Name");
            DropColumn("dbo.Sales", "Quantity");
            DropColumn("dbo.Customers", "ZipCode");
            DropColumn("dbo.Customers", "UserId");
            DropColumn("dbo.Orders", "Customer_UserId");
            AddPrimaryKey("dbo.Customers", "CustomerID");
            CreateIndex("dbo.Orders", "Product_ProductID");
            CreateIndex("dbo.Orders", "CustomerID");
            AddForeignKey("dbo.Orders", "CustomerID", "dbo.Customers", "CustomerID", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "Product_ProductID", "dbo.Products", "ProductID");
        }
    }
}
