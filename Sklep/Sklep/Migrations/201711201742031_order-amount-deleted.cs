namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderamountdeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "Amount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Amount", c => c.Single(nullable: false));
        }
    }
}
