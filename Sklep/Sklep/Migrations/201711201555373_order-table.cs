namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordertable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sales", "Order_UserId", "dbo.Orders");
            DropIndex("dbo.Sales", new[] { "Order_UserId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropColumn("dbo.Sales", "OrderID");
            RenameColumn(table: "dbo.Sales", name: "Order_UserId", newName: "OrderID");
            DropPrimaryKey("dbo.Orders");
            AlterColumn("dbo.Sales", "OrderID", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Orders", "OrderID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Orders", "OrderID");
            CreateIndex("dbo.Sales", "OrderID");
            CreateIndex("dbo.Orders", "UserId");
            AddForeignKey("dbo.Sales", "OrderID", "dbo.Orders", "OrderID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sales", "OrderID", "dbo.Orders");
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Sales", new[] { "OrderID" });
            DropPrimaryKey("dbo.Orders");
            AlterColumn("dbo.Orders", "OrderID", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Sales", "OrderID", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.Orders", "UserId");
            RenameColumn(table: "dbo.Sales", name: "OrderID", newName: "Order_UserId");
            AddColumn("dbo.Sales", "OrderID", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "UserId");
            CreateIndex("dbo.Sales", "Order_UserId");
            AddForeignKey("dbo.Sales", "Order_UserId", "dbo.Orders", "UserId");
        }
    }
}
