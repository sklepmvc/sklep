namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usermodification : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Customer_UserId", "dbo.Customers");
            DropForeignKey("dbo.Customers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Sales", "OrderID", "dbo.Orders");
            DropIndex("dbo.Sales", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "Customer_UserId" });
            DropIndex("dbo.Customers", new[] { "UserId" });
            DropPrimaryKey("dbo.Orders");
            AddColumn("dbo.Sales", "Order_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Orders", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Orders", "OrderID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Orders", "UserId");
            CreateIndex("dbo.Sales", "Order_UserId");
            CreateIndex("dbo.Orders", "UserId");
            AddForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Sales", "Order_UserId", "dbo.Orders", "UserId");
            DropColumn("dbo.Orders", "CustomerID");
            DropColumn("dbo.Orders", "Customer_UserId");
            DropTable("dbo.Customers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        CustomerID = c.Int(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                        Address = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            AddColumn("dbo.Orders", "Customer_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Orders", "CustomerID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sales", "Order_UserId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Sales", new[] { "Order_UserId" });
            DropPrimaryKey("dbo.Orders");
            AlterColumn("dbo.Orders", "OrderID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Orders", "UserId");
            DropColumn("dbo.Sales", "Order_UserId");
            AddPrimaryKey("dbo.Orders", "OrderID");
            CreateIndex("dbo.Customers", "UserId");
            CreateIndex("dbo.Orders", "Customer_UserId");
            CreateIndex("dbo.Sales", "OrderID");
            AddForeignKey("dbo.Sales", "OrderID", "dbo.Orders", "OrderID", cascadeDelete: true);
            AddForeignKey("dbo.Customers", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Orders", "Customer_UserId", "dbo.Customers", "UserId");
        }
    }
}
