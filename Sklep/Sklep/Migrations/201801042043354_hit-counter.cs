namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hitcounter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HitCounters",
                c => new
                    {
                        HitCounterID = c.Int(nullable: false, identity: true),
                        Counter = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HitCounterID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HitCounters");
        }
    }
}
