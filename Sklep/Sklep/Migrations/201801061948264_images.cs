namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class images : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Image", c => c.Binary());
            AddColumn("dbo.Products", "BigImage", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "BigImage");
            DropColumn("dbo.Products", "Image");
        }
    }
}
