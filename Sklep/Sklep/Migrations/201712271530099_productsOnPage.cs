namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productsOnPage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ProductsOnPage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ProductsOnPage");
        }
    }
}
