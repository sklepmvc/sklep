namespace Sklep.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "PaymentMethod", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "ShipmentMethod", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "State", c => c.String());
            AlterColumn("dbo.Orders", "ShipmentMethod", c => c.String());
            AlterColumn("dbo.Orders", "PaymentMethod", c => c.String());
        }
    }
}
