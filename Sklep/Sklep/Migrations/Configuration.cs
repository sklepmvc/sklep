namespace Sklep.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Sklep.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Sklep.Models.ApplicationDbContext";
        }

        protected override void Seed(Sklep.Models.ApplicationDbContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("admin"))
                roleManager.Create(new IdentityRole("admin"));

            if (!roleManager.RoleExists("user"))
                roleManager.Create(new IdentityRole("user"));


            if (!(context.Users.Any(u => u.UserName == "admin@admin.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { Email = "admin@admin.com", PasswordHash = "AC8PDOBbvWNny6bd1SFKkjsgvg4dq4FAaGdz+KYR4qpPtwng3oLmfvu1OpkdVT7YRg==", UserName = "admin@admin.com" };
                userManager.Create(userToInsert);
                userManager.AddToRole(userToInsert.Id, "admin");
            }

            if (!(context.HitCounters.Any())) context.HitCounters.Add(new HitCounter() { HitCounterID=1, Counter=0 });
        }
    }
}
